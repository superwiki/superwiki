import { SuperwikiPage } from './app.po';

describe('superwiki App', function() {
  let page: SuperwikiPage;

  beforeEach(() => {
    page = new SuperwikiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
